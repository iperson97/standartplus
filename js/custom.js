$(document).ready(function() {
	$(function($){
		$('[name="Телефон"]').mask("+7(999) 999-9999");
	});
	
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$('.js-overlay-thank-you').fadeIn();
			//alert('Сообщение успешно отправлен!');
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});
	
	$('.js-close-thank-you').click(function() { // по клику на крестик
		$('.js-overlay-thank-you').fadeOut();
	});

	$(document).mouseup(function (e) { // по клику вне попапа
	    var popup = $('.popup');
	    if (e.target!=popup[0]&&popup.has(e.target).length === 0){
	        $('.js-overlay-thank-you').fadeOut();
	    }
	});
});